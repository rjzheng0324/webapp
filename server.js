// Babel ES6/JSX Compiler
require('babel-register');
var React = require('react');
var ReactDOM = require('react-dom/server');
var Router = require('react-router');
var express = require('express');
var path = require('path');
var routes = require('./routes');
var bodyParser = require('body-parser');
var swig  = require('swig');
var webpack = require("webpack");
var webpackConfig = require('./webpack.config');
var compiler = webpack(webpackConfig);

var app = express();

// app.use(require('webpack-dev-middleware')(compiler, {
//   publicPath: webpackConfig.output.publicPath,
//   contentBase: webpackConfig.output.path,
//   hot: true,
//   historyApiFallback: true,
//   filename: path.resolve(webpackConfig.output.path, webpackConfig.output.filename),
//   headers: { "Access-Control-Allow-Origin": "*" },
//   stats: { colors: true }
// }));
// app.use(require('webpack-hot-middleware')(compiler));

app.use(express.static(path.join(__dirname)));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

const PORT = process.env.PORT || 8080;

var WebpackDevServer = require("webpack-dev-server");

// ****** Comment out this block to use socket.io
// Block Begins
var server = new WebpackDevServer(compiler, {
  contentBase: webpackConfig.output.path,
  hot: true,
  historyApiFallback: true,
  filename: path.resolve(webpackConfig.output.path, webpackConfig.output.filename),
  publicPath: webpackConfig.output.publicPath,
  headers: { "Access-Control-Allow-Origin": "*" },
  stats: { colors: true }
});
// Block ends

// ****** Uncomment this block to use socket.io
// Block Begins
// var server = require('http').createServer(app);
// var io = require('socket.io')(server);
// Block Ends

server.listen(PORT);

// ******* Uncomment this block to use socket.io
// Comment Begins
// io.on('connection', function(socket){
//   console.log("Welcome to the Web Portal");
//
//   socket.on('disconnect', function () {
//     console.log('Goodbye');
//   });
// });
//
// app.post("/notify", function(req, res) {
//   var title = req.body.title;
//   var description = req.body.description;
//   io.emit('notification', title + '\n'+ description);
//   res.send("Notification received");
// });
// Block Ends

//REACT MIDDLEWARES
// app.use(function(req, res) {
//   Router.match({ routes: routes.default, location: req.url }, function(err, redirectLocation, renderProps) {
//     if (err) {
//       res.status(500).send(err.message)
//     } else if (redirectLocation) {
//       res.status(302).redirect(redirectLocation.pathname + redirectLocation.search)
//     } else if (renderProps) {
//       var html = ReactDOM.renderToStaticMarkup(React.createElement(Router.RouterContext, renderProps));
//       var page = swig.renderFile('index.html', { html: html});
//       res.status(200).send(page);
//     } else {
//       res.status(404).send('Page Not Found')
//     }
//   });
// });
