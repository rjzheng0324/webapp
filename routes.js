/* Node Modules */
import React from 'react';
import { Route, IndexRoute } from 'react-router';

/* Components */
import Login from './src/components/login';
import Dashboard from './src/components/dashboard';
import App from './src/components/app';

export default (
	<Route path="/" component={App}>
		<IndexRoute component={Login} />
		<Route path="dashboards/:name" component={Dashboard} />
	</Route>
);