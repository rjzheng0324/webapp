/* Actions */
import { FETCH_DASHBOARDS } from '../actions/index';
import { FETCH_DASHBOARD } from '../actions/index';

const INITIAL_STATE = { all: [], dashboard: null };

export default function(state=INITIAL_STATE, action) {
	switch(action.type) {
	case FETCH_DASHBOARDS:
		return { ...state, all: action.payload.data.dashboards };
	case FETCH_DASHBOARD:
		return { ...state, dashboard: action.payload.data };
	default:
		return state;
	}

}