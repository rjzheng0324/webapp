/* Node Modules */
import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

/* Reducers */
import DashboardsReducer from './reducer_dashboards';

const rootReducer = combineReducers({
	dashboards: DashboardsReducer,
	form: formReducer
});

export default rootReducer;
