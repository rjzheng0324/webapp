/* Node Modules */
import axios from 'axios';
import * as qs from 'qs';

/* Return Type Keyword Constants */
export const LOGIN = 'LOGIN';
export const FETCH_DASHBOARDS = 'FETCH_DASHBOARDS';
export const FETCH_DASHBOARD = 'FETCH_DASHBOARD';

/* Root URL for API calls */
const ROOT_URL = 'http://192.168.20.90:8000/web/'
// const ROOT_URL = 'http://192.168.0.4:8000/web/'

/* Action to authenticate the user */
export function login(props) {

	const request = axios.post(`${ROOT_URL}login/`, qs.stringify(props) );

	return {
		type: LOGIN,
		payload: request
	};
}

/* Action to fetch all Kibana dashboards */
export function fetchDashboards() {
	const request = axios.get(`${ROOT_URL}dashboards/all/`);

	return {
		type: FETCH_DASHBOARDS,
		payload: request
	}
}

export function fetchDashboard(name){
	var param = { board: name };
	const request = axios.post(`${ROOT_URL}dashboards/board/`, qs.stringify(param));
	return {
		type: FETCH_DASHBOARD,
		payload: request
	}
}

export function hello(name) {
	console.log(name);
	return name;
}
