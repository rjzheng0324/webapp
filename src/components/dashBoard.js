/* Node Modules */
import React, { Component } from 'react';
import { DropdownButton, Nav, NavItem, NavDropdown, MenuItem, ButtonToolbar, ListItem } from 'react-bootstrap';
import { AlertList, Alert, AlertContainer } from "react-bs-notifier";
import { Link } from 'react-router';
import { connect } from 'react-redux';

import Alerts from './alert';
import SearchBar from './searchBar';


/* Actions */
import { fetchDashboards } from '../actions/index';
import { fetchDashboard } from '../actions/index';

class DashBoard extends Component {

  componentWillMount() {
    this.props.fetchDashboards();
    this.props.fetchDashboard(this.props.params.name);
  }

  renderBoardList() {
    return this.props.dashboards.map((dashboard) => {
      return(
        <MenuItem 
          key={dashboard.name} 
          href={dashboard.name}
        >{dashboard.name}
        </MenuItem>
      ); 
      
    });
  }

  renderBoard(source) {
    return <iframe frameBorder="0" scrolling="no" src={source} height="950px" width="1100px"></iframe>
  }
 
	render() {
		return(
      <div>
        <Nav className="navbar navbar-inverse navbar-fixed-top" role="navigation">
          <div className="container">
          	<a className="navbar-brand" href="#">
              <img className="logo" src="../../img/ZitoVault_White_Red.png" alt="ZitoVault"></img>
            </a>
            
            <Nav className="nav navbar-nav">
                <NavItem href="#">Home</NavItem>
                <NavDropdown title="DashBoards" id="nav-dropdown">
                  {this.renderBoardList()}
                </NavDropdown>
                <NavItem href="#">Threats</NavItem>
                <NavItem href="#">Settings</NavItem>
            </Nav>

            <div>
              <SearchBar />
            </div>

            <Nav className="nav navbar-nav navbar-right">
              <NavItem href="#"><span className="glyphicon glyphicon-time"></span> Time</NavItem>
              <NavItem href="/">Logout</NavItem>
            </Nav>
          </div>
        </Nav>

        <div>
          {this.renderBoard(this.props.dashboard)}
        </div>
      </div>
		);
	}
}

function mapStateToProps(state) {
  return { 
    dashboards: state.dashboards.all,
    dashboard: state.dashboards.dashboard
  }
}

export default connect(mapStateToProps, { fetchDashboards, fetchDashboard })(DashBoard);