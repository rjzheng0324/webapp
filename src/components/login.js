/* Node Modules */
import React, { Component, PropTypes } from 'react';
import { reduxForm } from 'redux-form';

/* Actions */
import { login } from '../actions/index';

class Login extends Component {

  static contextTypes = {
    router: PropTypes.object
  };

  onSubmit(props) {
    this.props.login(props)
      .then((response) => {
        // if(response.payload.data == "Login success"){
          this.context.router.push('dashboards/Z-Heartbeat');
        // }
      });
  }

  render() {

    const { fields: { email, password }, handleSubmit } = this.props;

		return (
      <div className="account-wall">
        <img className="profile-img" src="../../img/zv_logo.png"
            alt=""></img>
        <form onSubmit={handleSubmit(this.onSubmit.bind(this))} className="form-signin">
        	<input type="text" className="form-control" placeholder={email.touched ? email.error : email.error } required autoFocus {...email} />
        	<input type="password" className="form-control" placeholder={password.touched ? password.error : password.error } required {...password} />
        	<button className="btn btn-lg btn-color btn-block" type="submit">
          	Sign in
          </button>
          <button className="btn btn-lg btn-color btn-block">
          	Create an account
          </button>
        	<label className="checkbox pull-left">
          	<input type="checkbox" value="remember-me"></input>
          	<span className="text-center text-color-red">Remember me</span>
        	</label>
        </form>
      </div>
		);
	}
}

function validate(values) {
  const errors = {};

  if(!values.email) {
    errors.email = 'Enter your email address';
  }

  if(!values.password) {
    errors.password = 'Enter your password';
  }

  return errors;
}

export default reduxForm({
  form: 'LoginForm',
  fields: ['email', 'password']
}, null, { login })(Login);
