/* Node Modules */
import React, { Component } from 'react';
import { Nav } from 'react-bootstrap';
import { NavItem } from 'react-bootstrap';
import { MenuItem } from 'react-bootstrap';

class SearchBar extends Component {
	
	// Constructor of the search bar
	constructor(props) {
		// Super the parent constructor
		super(props);

		// State variables
		this.state = {
			term: ''
		};
	}

	// Renders the JSX component
	render() {
		return (
			<div className="col-sm-4 searchbar">
	      <div className="input-group stylish-input-group">
	          <input type="text" className="form-control"  placeholder="Search" ></input>
	          <span className="input-group-addon">
	              <button type="submit">
	                  <span className='glyphicon glyphicon-search'></span>
	              </button>  
	          </span>
	      </div>
	    </div>
		);
	}
}

export default SearchBar;