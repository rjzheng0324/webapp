// Babel ES6/JSX Compiler
require('babel-register');
var _ = require('underscore');
var React = require('react');
var ReactDOM = require('react-dom/server');
var Router = require('react-router');
var express = require('express');
var path = require('path');
var routes = require('./routes');
var async = require('async');
var bodyParser = require('body-parser');
var swig  = require('swig');
const renderToString = require('react-dom/server').renderToString;
const hist = require('history');
require('babel-core/register')({
    presets: ['es2015', 'react']
})

//SERVER
var app = express();
app.set('port', 8080);

//EXPRESS MIDDLEWARES
app.use(express.static(path.join(__dirname)));
app.set('view engine', 'html');
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))
// parse application/json
app.use(bodyParser.json({ extended: true }))

//SOCKET.IO CONFIG
var server = require('http').createServer(app);
var io = require('socket.io')(server);
//Whenever someone connects this gets executed
io.on('connection', function(socket){
  console.log("Welcome to the Web Portal");

  socket.on('disconnect', function () {
    console.log('Goodbye');
  });

});


//AJAX API ROUTES
/**
 * GET /api/posts/top
 * Return top 100 most recent posts
*/
app.post("/notify", function(req, res) {
  var title = req.body.title;
  var description = req.body.description;
  io.emit('notification', title + '\n'+ description);
  res.send("Notification received");
})

//REACT MIDDLEWARES
// app.use(function(req, res) {
//   Router.match({ routes: routes.default, location: req.url }, function(err, redirectLocation, renderProps) {
//     if (err) {
//       res.status(500).send(err.message)
//     } else if (redirectLocation) {
//       res.status(302).redirect(redirectLocation.pathname + redirectLocation.search)
//     } else if (renderProps) {
//       var html = ReactDOM.renderToStaticMarkup(React.createElement(Router.RouterContext, renderProps));
//       var page = swig.renderFile('index.html', { html: html});
//       res.status(200).send(page);
//     } else {
//       res.status(404).send('Page Not Found')
//     }
//   });
// });



server.listen(app.get('port'), function() {
  console.log('ZitoVault Ignite Express server listening on port: ' + app.get('port'));
});